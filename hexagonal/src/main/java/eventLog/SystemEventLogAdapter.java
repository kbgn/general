package eventLog;

import domain.Pushpin;

import java.util.logging.Logger;

public class SystemEventLogAdapter implements PushpinEventLogPort {

    private static final Logger LOGGER = Logger.getLogger(SystemEventLogAdapter.class.getName());
    @Override
    public void addPushpin(Pushpin pushpin) {
        LOGGER.info("Pushpin was added with image URL : "+ pushpin.getImageUrl());
    }

    @Override
    public void deletePushpin(Pushpin pushpin) {
        LOGGER.info("Deleted pushpin id is : "+ pushpin.getId());
    }

    @Override
    public void findPushpin(Pushpin pushpin) {
        LOGGER.info("Pushpin found with id : "+ pushpin.getId());
    }
}
