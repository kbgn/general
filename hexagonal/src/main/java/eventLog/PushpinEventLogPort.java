package eventLog;

import domain.Pushpin;

public interface PushpinEventLogPort {
    /**
     * Pushpin is added
     */
    void addPushpin(Pushpin pushpin);

    /**
     * Pushpin deleted
     */
    void deletePushpin(Pushpin pushpin);

    /**
     * Find Pushpin
     */
    void findPushpin(Pushpin pushpin);
}
