package service;

import domain.PushpinService;

import java.util.Scanner;

public interface PushpinConsolePort {
    void addPushpin(PushpinService pushpinService, Scanner scanner);
    void deletePushpin(PushpinService pushpinService, Scanner scanner);
}
