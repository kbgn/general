package service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import domain.PushpinService;

import java.util.Scanner;
import java.util.logging.Logger;

public class PushpinMainApplication {
    private static final Logger LOGGER = Logger.getLogger(PushpinMainApplication.class.getName());

    /**
     * Program entry point
     */
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new PushpinModule());
        PushpinService service = injector.getInstance( PushpinService.class);
        try (final Scanner scanner = new Scanner(System.in)) {
            boolean exit = false;
            while (!exit) {
                printMenu();
                String cmd = scanner.next();
                PushpinConsolePort pushpinConsolePort = new PushpinConsoleAdapter(LOGGER);
                if ("1".equals(cmd)) {
                    pushpinConsolePort.addPushpin(service, scanner);
                } else if ("2".equals(cmd)) {
                    pushpinConsolePort.deletePushpin(service, scanner);
                } else if ("3".equals(cmd)) {
                    exit = true;
                } else {
                    LOGGER.info("Unknown command");
                }
            }
        }
    }

    private static void printMenu() {
        LOGGER.info("");
        LOGGER.info("### Pushpin Service Console ###");
        LOGGER.info("(1) Add Pushpin");
        LOGGER.info("(2) Delete Pushpin by Id");
        LOGGER.info("(3) Exit");
    }

}
