package service;

import com.google.inject.AbstractModule;
import database.PushpinPort;
import database.PushpinSystemAdapter;
import eventLog.PushpinEventLogPort;
import eventLog.SystemEventLogAdapter;

public class PushpinModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(PushpinPort.class).to(PushpinSystemAdapter.class);
        bind(PushpinEventLogPort.class).to(SystemEventLogAdapter.class);
    }
}
