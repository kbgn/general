package service;

import domain.Pushpin;
import domain.PushpinService;

import java.util.Scanner;
import java.util.logging.Logger;

public class PushpinConsoleAdapter implements PushpinConsolePort {
    private final Logger logger;

    public PushpinConsoleAdapter(Logger logger) {
        this.logger = logger;
    }


    @Override
    public void addPushpin(PushpinService pushpinService, Scanner scanner) {
        logger.info( "Please enter Pushpin image URL: " );
        String imageUrl = scanInput( scanner );
        logger.info( "Please add tags (comma separated) for the image: " );
        String tags = scanInput( scanner );

        Pushpin pushpin = Pushpin.create(imageUrl, tags);
        pushpinService.addPushpin(pushpin);
        logger.info( "Pushpin added successfully with id : "+ pushpin.getId() );
    }

    @Override
    public void deletePushpin(PushpinService pushpinService, Scanner scanner) {
        logger.info( "Please enter Pushpin id to delete: " );
        int pushpinId = scanIntegerInput( scanner );
        Pushpin pushpin = pushpinService.findPushpinById(pushpinId).get();
        pushpinService.deletePushpin(pushpin);
        logger.info( "Pushpin deleted successfully with id : "+ pushpin.getId() );
    }

    private String scanInput(Scanner scanner) {
        System.out.print( "> " );
        return scanner.next();
    }

    private int scanIntegerInput(Scanner scanner) {
        System.out.print( "> " );
        return scanner.nextInt();
    }
}
