package database;

import domain.Pushpin;

import java.util.Optional;


public interface PushpinPort {

    /**
     * Save Pushpin
     */
    Optional<Pushpin> save(Pushpin pushpin);

    /**
     * Delete pushpin by id
     */
    void deletePushpin(int id);

    /**
     * Find Pushpin by id
     */
    Optional<Pushpin> findById(int id);
}
