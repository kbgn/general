package database;

import domain.Pushpin;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class PushpinSystemAdapter implements PushpinPort {

    private static Map<Integer, Pushpin> pushpins = new HashMap<>();
    @Override
    public Optional<Pushpin> save(Pushpin pushpin) {
        Integer id = Integer.valueOf((int)Math.random());
        pushpin.setId(id);
        pushpins.put(id, pushpin);
        return Optional.of(pushpin);
    }

    @Override
    public void deletePushpin(int id) {
        Pushpin pushpin = pushpins.get(id);
        if (Objects.nonNull(pushpin)) {
          pushpins.remove(pushpin.getId());
        }
    }

    @Override
    public Optional<Pushpin> findById(int id) {
        Pushpin pushpin = pushpins.get(id);
        if (pushpin == null) {
            return Optional.empty();
        } else {
            return Optional.of(pushpin);
        }
    }
}
