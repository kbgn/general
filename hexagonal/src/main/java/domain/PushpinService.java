package domain;

import com.google.inject.Inject;
import database.PushpinPort;
import eventLog.PushpinEventLogPort;

import java.util.Optional;

public class PushpinService {
    private final PushpinPort pushpinPort;
    private final PushpinEventLogPort pushpinEventLogPort;

    /**
     * Constructor
     */
    @Inject
    public PushpinService(PushpinPort pushpinPort, PushpinEventLogPort pushpinEventLogPort) {
        this.pushpinPort = pushpinPort;
        this.pushpinEventLogPort = pushpinEventLogPort;
    }

    public Optional<Pushpin> addPushpin(Pushpin pushpin) {
        Optional<Pushpin> optional = pushpinPort.save(pushpin);
        if (optional.isPresent()) {
            pushpinEventLogPort.addPushpin(optional.get());
        }
        return optional;
    }

    public Optional<Pushpin> findPushpinById(int pushpinId) {
        Optional<Pushpin> optional = pushpinPort.findById(pushpinId);
        if (optional.isPresent()) {
            pushpinEventLogPort.findPushpin(optional.get());
        }
        return optional;
    }

    public void deletePushpin(Pushpin pushpin) {
        pushpinPort.deletePushpin(pushpin.getId());
    }

}


