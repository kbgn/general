package domain;

public class Pushpin {
    private int id;
    String imageUrl;
    String tags; // comma separated tags

    public Pushpin(String imageUrl, String tags) {
        this.imageUrl = imageUrl;
        this.tags = tags;
    }

    public static Pushpin create(String imageUrl, String tags) {
        return new Pushpin(imageUrl, tags);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
