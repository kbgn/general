# Hexagonal Architecture

A sample project to demonstrate hexagonal architecture. The use case is to demonstrate adding and deleting pushpins.

## Getting Started

Clone this project to your local repository. The project uses gradle as the build tool.

`./gradlew clean `

### Prerequisites

Need to have Java 8

## Running

`./gradlew run`

## Built With

* [Gradle] - Dependency Management and build tool

## Author

* **Booshitha Krishnan**  

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details